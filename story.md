---
repository: https://github.com/jfcmacro/commandlambdastream.git
tag: main-v2.0
---
<!-- DONE Step 1 Readme.md it looks raw format instead of using Markdown. It looks ok -->

# Command Lambda Stream Story

**To read the story**: https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fcoffee-machine-story-java

**To code yourself**: https://git.epam.com/Refactoring-Stories/coffee-machine-story-java

**Estimated reading time**: 50 minutes

## Story Outline
Programming languages were defined to help us to describe a computation,
a computation can be understood as a transformation of a value into another
value. The transformation is described as a well-formed complete program.
That program is defined as a static program, which means if the computation is
changed, the program must be rewritten to fit the new computation. This is
common in almost all programming languages that belong to the
Imperative Paradigm, the program must be rewritten. But, In paradigm
declarative, the program is seen as data (a value) and can be transformed
and rewritten dynamically as another computation.

Object-Oriented Programming and, in particular, Java is an Imperative
Programming Language, this means that the computation described in a
source code is defined in a static manner. If you need to describe a
computation dynamically, you can use the Command Design Pattern: “Encapsulate a
request as an object, thereby letting you parameterize clients with different
request, queue or log request, and support undoable operations”
[Gamma et al,1995]. The Command pattern can be used as parameterized
computation, which enables us to change computation dynamically without
recompiling the application.

As we already said, there is another programming paradigm that enables using a
program as data, the declarative paradigm in particular functional programming
can use a function to describe computation, but also uses a function as data
that can be passed to a function as a parameter or returned as a result of a
computation. Because of this, most object-oriented programming language
designers have incorporated that characteristic into current Object-Oriented
Programming Languages like Java, C++, and others.

In this story, we will show this evolution, through an application that
implements the main functions of a database based on spreadsheets, through the
following types of computation: forEach, filter, map and, reduce. This service
will be implemented by starting with the design pattern called Command, in its
first version in a non-generic way, and in the second we will improve it with
generic data type mechanisms. Then, on the third version, we will do it through
lambda expressions. In the fourth version, we will present lambda expressions
in a more simple through method references. Finally, we will show how streams
make it easy for us to implement these methods.

## Story Organization
**Story Branch**: main

> `git checkout main`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #command_design_pattern, #generics, #lambda_functions, #streams

---
focus:
---

### Command Lambda Stream Story

This story will tell a development, how to go from a world completely
objective in one that can be worked both functionally as
object-oriented.

Our story will start by showing how to implement a basic service of a
table-based database similar to the one implemented in the
spreadsheets through a service that will allow you to iterate from
different ways:

* `forEach` apply a computation with side effects to each database
  item.
* `filter` through a predicate determines which elements are left in
   the database.
* `map` iterates over each item in the database producing a new base
   with a different type.
* `reduce` iterates over the database values producing a result.

We will implement these functions like this:

* Through the Command design pattern without generics.
* Through the Command design pattern with generics.
* Through lambdas expressions that our command replaces.
* Through lambda expressions with reference methods.
* Through the facilities built into *streams*.

---
focus: src/main/java/com/epam/rd/cls/SalesSummaryRow.java
---

### Database description

<!-- DONE IT It looks stranger - rewrite it!
          Some mistyping on this step - The explanation was rewritten. -->

Our database design is based on a set of sales summarized on a table, where each row ([`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)) contains the sales of one article ([`Articles`](src/main/java/com/epam/rd/cls/Article.java)) on a region ([`Region`](src/main/java/com/epam/rd/cls/Region.java)) during a period of time. The database is generated through a helper class [`DBHelper`](src/main/java/com/epam/rd/cls/DBHelper.java) which returns a list of type values [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java), formally: `List<SalesSummaryRow>` is the type of our database data.

**Note:** Although we are already using generics with the type List, we do it because we do not want to add nongeneric code for use of lists, bloating unnecessary our code, which is already bigger than we expect.

---
focus: src/main/java/com/epam/rd/cls/Command.java
---

### A Command Way

Each one of the functions described above (foreach, filter, map, reduce) describes a computation,  the Interface [Command](src/main/java/com/epam/rd/cls/Command.java) indicates this computation.

The functions described above (`foreach`, `filter`, `map`, `reduce`) require computation. This is represented by the Interface [Command](src/main/java/com/epam/rd/cls/Command.java).

A class that implements this interface [Command](src/main/java/com/epam/rd/cls/Command.java), defines a concrete computation. This computation is defined by a function that does not receive parameters or return a value as shown in the continuation.

```java
public void function() {
...
}
```

A class that implements that interface Command,  enables us to define a function as data, that can be passed as a parameter or returned as a result of a computation, this enables us to define dynamically a behavior.

```java
Command command = new ClassImplementsCommand();
command.execute();
```

It can be passed as a parameter:

```java
public void apply(Command command) {
   command.execute();
}
```

It can be returned from a method:

```java
public Command getCommand(...) {
  ...
}
```

---
focus: src/main/java/com/epam/rd/cls/ForEachCmd.java
---

### For Each Command `ForEachCmd`

This service will iterate over each element of the database (table) and will produce a side effect, that side effect could be an input and output operation or produce a new database (table). The classes that implement the interface [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) will traverse the database by applying a customized procedure. In this case, the procedure is defined by the interface [`SetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/SetSlsSumRwCmd.java). This interface implements the interface [Command](src/main/java/com/epam/rd/cls/Command.java); it does this to receive a value of type [SalesSummaryRow](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) in each evaluation.

The signature of the procedure is

```java
public void function(SalesSummaryRow salesSummaryRow) {
...
}
```

Because we have decided to avoid extensive using generics, we have to implement for each different type, it is primitive or object, a specific class that wrapper that type; for instance, for types: `Integer` and `Double`, we have defined each one as a concrete class: [`ForEachDblCmd`](src/main/java/com/epam/rd/cls/ForEachDblCmd.java)(`Double`), [`ForEachIntCmd`](src/main/java/com/epam/rd/cls/ForEachIntCmd.java) (`Integer`); both classes share the same code, the only thing that change is its inner type. 

We know that a better strategy is using generics. Still, in this particular case, we want to use the original definition of Command Design Pattern that does not use generics, and we have kept the same guidance for every has to do with Design Patterns, for the moment, the next step is using generics to avoid the current explosion of concrete classes.

<!-- DONE IT  - It may be worth noting that this problem can be solved using generics -- See the above paragrah. -->

---
focus: src/main/java/com/epam/rd/cls/FilterCmd.java
---

<!-- TODO - The following sentence looks strange. rewrite it. -->

### The Filter service

<!-- DONE IT - Insert a space character at FilterCmd -->

<!-- DONE IT - This sentences looks stranger, rewrite it -->

<!-- DONE IT - The implementation doesn't match the following description return GetBoolSetSlsSumRwCmd -->

The main idea of this service is to select elements of the database (Table) that meet a specific predicate (or criteria). This service is defined by the class [`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCommand.java), its purpose is to produce a new database (Table). The [`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCommand.java) extends the interface [GetListSlsSumRwCmd](src/main/java/com/epam/rd/cls/GetListSlsSumRwCmd.java), which in turn extends from [Command](src/main/java/com/epam/rd/cls/Command.java). The database (`List<SalesSummaryRow>`) and its predicate ([GetBoolSetSlsSumRwCmd](src/main/java/com/epam/rd/cls/GetBoolSetSlsRwCmd.java)) are received by its constructor.
The predicate is an implementation of the interface [GetBoolSetSlsSumRwCmd](src/main/java/com/epam/rd/cls/GetBoolSetSlsRwCmd.java); the interface takes any SalesSummaryRow [SalesSummaryRow](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) and returns a boolean value indicating whether that data row accomplishes a specific criterion or not. Below is the signature of the interface [GetBoolSetSlsSumRwCmd](src/main/java/com/epam/rd/cls/GetBoolSetSlsRwCmd.java):

```java
public boolean predicate(SalesSummaryRow salesSummaryRow) {
...
}
```

---
focus: src/main/java/com/epam/rd/cls/MapIntegerCommand.java
---

### The service Map

The main objective of the mapping service is to transform a type
element
[`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)
into any other type. For example, transform [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) into a
value of type integer (`int`). This would require a function that has the
following signature:

```java
public int fromTo(SalesSummaryRow salesSummaryRow) {
...
}
```

This function will be applied to each element, producing a new value as shown in the previous example when it returns a value of type int. The class that contains this method (function) must implement the interface GetIntSetSlsSumRwCmd [`GetIntSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetIntSetSlsSumRwCmd.java); it is encharged to execute a function that receives a value of type [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java) and returns a value of type primitive integer (int); but, thanks to the java's boxing mechanism, it allows getting a value of type Integer.

The implementation will depend on the return type, as shown in [` MapListSlsSumRwIntCmd`](src/main/java/com/epam/rd/cls/MapListSlsSumRwIntCmd.java), which returns an `Integer` type, and [`MapListSlsSumRwDblCmd`](src/main/java/com/epam/rd/cls/MapListSlsSumRwDblCmd.java), which produces a `Double` type; if you want to return a different type, you must implement a new class that returns that type. You can see that the implementation walks through the database, applies the transform function `fromTo` to each element, and get the new database, as seen in the code [` MapListSlsSumRwIntCmd`](src/main/java/com/epam/rd/cls/MapListSlsSumRwIntCmd.java).

---
focus: src/main/java/com/epam/rd/cls/ReduceDoubleCommand.java
---

### The service reduce

The purpose of this service is to condense the database into a value
of a new kind. In this particular case, it will take the database from
Sales and condense it into a value of type `double`. It will be
implemented as a service through the class
[ReduceDoubleCommand](src/main/java/com/epam/rd/cls/ReduceDoubleCommand.java).

This service is carried out, as they have been done with the rest
services traversing the database but in this case an operator that has
the following signature:


```java
public double operator(SalesSummaryRow salesSummaryRow,
                       double value) {
...
}
```

This is responsible for receiving an element from the database and a
value of type `double`, in the body of the function, it will take care
of extracting or compute from this a value of type `double` and apply
the operator to get a result. It will do it on each of the elements of
the database, to finally reduce to a single value of type `double`.

The operator is implemented through the interface
[GetDoubleSetDoubleSetSalesSummaryRowCommand](src/main/java/com/epam/rd/cls/GetDoubleSetDoubleSetSalesSummaryRowCommand.java).

Again, the database is traversed, applying to each element this
operator as can be seen in the implementation of the function
[execute()](src/main/java/com/epam/rd/cls/ReduceDoubleCommand.java:24-31).

The operator is implemented through the interface GetDblSetDblSetSlsSumRwCmd [`GetDblSetDblSetSlsSumRwCmd`](src/main/java/com/epam/rd/cls/GetDblSetDblSetSlsSumRwCmd.java). An important detail is observed in the constructor of ReduceDblCmd [`ReduceDblCmd`](src/main/java/com/epam/rd/cls/ReduceDblCmd.java); not only the database and a reference to the operator are received, but also the initial value is passed.

<!-- DONE IT - Too many changes and renames in one step. It is better to make such changes sequentially, step by step with the appropiate comments: From An important -- empty. -  I rewrote and rephrased the page, which improved its readability. -->
<!-- DONE IT - The following block is not formatted - It was erased. -->

focus: src/main/java/com/epam/rd/cls/Main.java
---

### A little improving of the functions: `forEach`, `filter`, `map`, `reduce`

Although these commands receive other commands as their parameters,
their use requires a lot of repeated code, knowing that in case the
different implementations of said commands also have still more
repeated code.

Some functions have been made that simplify this repetition of code.

These are seen in:

* [`forEach`](src/main/java/com/epam/rd/cls/Main.java:7-12)
* [`filter`](src/main/java/com/epam/rd/cls/Main.java:28-35)
* [`map`](src/main/java/com/epam/rd/cls/Main.java:37-45)
* [`reduce`](src/main/java/com/epam/rd/cls/Main.java:57-67)

---
focus:
---

### Summary of a Command way

Although we can implement the services on the database Extending the
use of the *` Command`* design pattern has several Obvious problems:

   * There is a lot of repeated code due to the lack of handling
     generality.
   * If a function requires a very high number of parameters,
     you will have to create a large number of interfaces that support both
     each parameter as the different type it has, therefore
     causing an explosion of interfaces in the case that they are wanted
     work with more types of data.
   * It is not flexible to return values, as with the arguments
     that is passed requires new interfaces that allow
     collect those values

Therefore, in the next part, we will add features of generality to our
*`Command`*.

---
path: ...
branch: generics
focus: src/main/java/com/epam/rd/cls/Command.java:old:3-5
---

### It needs to be more generic, my Command!

We will start with the *` Command`* interface so that it can return
values, for this we have added the variable you type `R` to indicate
that it will return a value of any type.

---
focus: src/main/java/com/epam/rd/cls/SetCommand.java:5-7
---

### Passing generics arguments

Now that we can return a value from computation as well set an
argument. We achieve this by introducing the variable of type `T`
which is indicating that it can receive any type and this can be
dynamically input to the computation through the invocation of the
`set` method.

A function that receives a String and converts it to an integer could
be implemented like this:

<!-- TODO - The following block is not formatted -->
```java
SetCommand<String,Integer> sc = new SetCommand<String,Integer> {
   private String value;
   public void set(String value) {
     this.value = value;
   }
   public Integer execute() {
      return Integer.parseInt(value);
   }
}
```

To create, you can create instances of any type:
<!-- TODO - The following block is not formatted -->
```java
SetCommand<String,Integer> sc = new SetCommand<>() {
   private String value;
   public void setValue(String value) {
      this.value = value;
   }
   public Integer execute() {
      return Integer.parseInt(value);
   }
}
```

Now, do we have a more flexible version? A little more sense starting
from an interface we have infinite forms of representation, but what
happens if the previous code instead of returning a `Integer`returns a
`Double` you have to rewrite some very similar.

---
focus: src/main/java/com/epam/rd/cls/BiSetCommand.java
---

### More arguments

What if I want more arguments?

There are two alternatives. The first is to pass an array of Objects
and perform the conversion manually for each corresponding type, with
the code repeated in each case.

<!-- TODO - The following paragraph looks strange -->
The second interface is similar to the previous `SetCommand<T,R>`, but
with three arguments. What if we require more? Keep doing it the
same. Although this alternative may seem very complicated, it can do
it all at once with multiple type variable arguments and you'll have
enough flexibility, to write in one go.

---
focus: src/main/java/com/epam/rd/cls/ForEachCommand.java
---

### The forEach generic service

Having our generic base classes: `Command`, `SetCommand` and
`BiCommand`, we can implement each of the base services of data in a
generic way.

starting with the class
[ForEachCommand](src/main/java/com/epam/rd/cls/ForEachCommand.java). Is
class is parameterized in two elements: `T` which represents the
elements of the database and `R` which can be the value of return,
although for the proposed model this last parameter is not requires
and may be instantiated to a value of type `Void`.

<!-- TODO - Not a very good choice for links to relevant code -->
the builder of
[ForEachCommand](src/main/java/com/epam/rd/cls/ForEachCommand.java:10-14),
receives the database and the function to iterate over that value.

<!-- TODO - Changes are not displayed (old and new code) -->
It can be seen as
[ForEachCommand](src/main/java/com/epam/rd/cls/ForEachCommand.java:16-23)
is executed by iterating through each element, executing the function
internal represented by `SetCommmand`.

---
focus: src/main/java/com/epam/rd/cls/FilterCommand.java
---

### The generic `filter` service

Now let's look at the service implementation of `filter`.

The builder
[FilterCommand](src/main/java/com/epam/rd/cls/ForEachCommand.java:10-14),
very similar to the above implementation of `ForEachCommand`, but in
In this case, the argument containing the `SetCommand` is fixed in the
the second parameter which is of type `Boolean`, this represents a
function that gives a value of type `T` tells us if that value is
fulfilled or not with the predicate.

The following part shows the implementation of the function
[`execute`](src/main/java/com/epam/rd/cls/ForEachCommand.java:16-25),
this function returns a new database with values from the same type,
but that fulfills the predicate.

---
focus: src/main/java/com/epam/rd/cls/MapCommand.java
---

### The generic `map` service

The implementation of the mapping service follows a very similar line
to the two previous generic solutions. The determining elements are
the function represented by an instance of `SetCommand` it is not the
type is fixed and the result is of interest since this will be part of
the result that is a new database, we can observe this in the
implementation of the function
[`execute()`](src/main/java/com/epam/rd/cls/MapCommand.java:17-24),
which takes care of accumulating and adding each result to the return
list (`retList`).

---
focus: src/main/java/com/epam/rd/cls/ReduceCommand.java
---

### The generic `reduce` service

The `reduce` service has significant changes in the constructor of
[ReduceCommand](src/main/java/com/epam/rd/cls/ReduceCommand.java:12-18)
, not only does it receive the database, but it also receives a an
instance of the `BiSetCommand` interface that is the representation of
a operator, while receiving the default value of the operation.

The implementation of the function
[`execute`](src/main/java/com/epam/rd/cls/ReduceCommand.java:20-28)
It can be seen how, through the `BiSetCommand` type operator, it
allows "accumulate" the result in each traversal on the database and
at the end return said "accumulator".

---
focus: src/main/java/com/epam/rd/cls/Main.java
---

### Implementation Database Service (2 Part)

Let's look at the current implementation concerning the implementation
previous.

* [`forEach`](src/main/java/com/epam/rd/cls/Main.java:7-12:old)
* [`filter`](src/main/java/com/epam/rd/cls/Main.java:28-35:old)
* [`map`](src/main/java/com/epam/rd/cls/Main.java:37-55:old)
* [`reduce`](src/main/java/com/epam/rd/cls/Main.java:57-67:old)

In each of them, it can be seen that the new code is more generic and
this gives greater flexibility when making each of services.

---
focus: src/main/java/com/epam/rd/cls/Main.java:51-60
---

### Implementation of Predicates

The construction of a predicate or any other computation is done
implementing the `SetCommand` interface defining the types that go to
work, either via [classes
anonymous](src/main/java/com/epam/rd/cls/Main.java:51-60)
or defining a new class
[PredicateSouthCommand](src/main/java/com/epam/rd/cls/PredicateSouthCommand.java:5-16).

In the same way, the necessary computations can be implemented to be
used on the defined services: `forEach`, `filter`, `map` and `reduce`.

---
focus:
---

### Summary of a Generic Command Way.

We have simplified the construction of computations much more using
generics. But even though classes have been reduced to implement the
different ones, there are still problems:

* Precision, some implementations will require to be more verbose,
   although less than the non-generic ones, they are still very
   imprecise.
* Flexibility, since you have to use a lot of repeated code to define
   each computation.

---
branch: lambda
tag: lambda-v2.0
focus:
---

### A Lambda Expressions Way

<!-- TODO - The following paragraph is too dificult to understand -->
Using the Command design pattern allowed us to store a computation,
but this is an artificial construction. What I know requires is a new
constructor within the language that provides us to describe these
computations more precisely and flexibly.

The same purpose, of storing and using a computation can be achieve
through a defined concept within the programming functional, *lambda
expressions*.

A lambda expression is a segment of code that can be stored and run
later.

The following is the syntax to define a lambda function in Java.

<!-- TODO - The following text looks strange -->
```java
(Argumentos) -> Cuerpo de la función
```

If we compare it with a method

<!-- TODO - The following text looks strange -->
```java
public R nombreFuncion(Argumentos) {
    // Cuerpo de la función
}
```
<!-- TODO - The following text looks strange -->
Note that in lambda expressions ($\lambda$).

* There are no explicit access modifiers (`public`, `protected`,
   `private`) of the function.
* There is no explicit return type, this is inferred from the body of
   the function.
* It has no functions.
* `->` is an operator that separates the body arguments from the
   function.

> A lambda expression ($\lambda$) represents a computation.

---
focus: src/main/java/com/epam/rd/cls/Main.java:12-18
---

### Applying lambda expressions to database service (`forEach`)

Let's apply lambda expressions to the first element of the service our
database: `foreach`. If we remember, we use a procedure defined by the
`SetCommand<T,R>` interface in which we set the type parameter `R` to
the type `Void`, to make it a process. With lambda expressions, a
procedure is represented by the `Consumer<T>` interface, which is
defined as a functional interface `@FunctionalInterface`, which
accepts a value and returns no result.

To concretize this instance, we don't have to create a class
anonymous, or create a named class that implements this interface,
although you can if you wish, it is better to specify it to through a
lambda expression that does the same as seen
[here](src/main/java/com/epam/rd/cls/Main.java:57).

The lambda expression follows the following format:

```java
s -> System.out.println(s)
```

* If the parameter list contains a single parameter, you can omit
  parentheses.
* If the body of the lambda expression only contains a single
  instruction, braces may be omitted.
* The lambda expression has only one parameter.
* The `println` method returns `void`.
* Therefore complies with the `Consumer` interface.


---
focus: src/main/java/com/epam/rd/cls/Main.java:20-30
---

### Applying lambda expression to database service (`filter`)

In our previous implementation, it required concretizing an instance
of the `SetCommand<T,R>` interface with the variable `R` replaced with
a value of type `Boolean` to represent a predicate.

With lambda expression, there is an interface called `Predicate<T>`
that takes a value of type `T` evaluates it and determines whether or
not it satisfies the predicate.

As we did before, we are going to concretize an instance of
`Predicate<T>` using a lambda expression that takes a value and
produces a result of type
[`Boolean`](src/main/java/com/epam/rd/cls/Main.java:60).
The argument `s`, with no type set, is inferred to be of type
`SalesSummaryRow`, an own method is applied to it and it is compared,
whose result is as expected.

---
focus: src/main/java/com/epam/rd/cls/Main.java:32-40
---

### Applying lambda expression to database service (`map`)

The implementation of the map requires a computation that takes a
value of one type and produce another (not necessarily different)
type.

In our previous version, we concreted an instance of the interface
`SetCommand<T,R>`. With lambda, an instance of the interface is used
`Function<T,R>`, which accepts an argument of type `T` and returns a
value of type `R`.

In this case, we use a lambda expression as follows:

```java
s -> s.getInitialUnits()
```
Where the variable is of type `SalesSummaryRow` and transforms it into a value
integer type.

---
focus: src/main/java/com/epam/rd/cls/Main.java:42-50
---

### Applying lambda expressioin to database service (`reduce`)

The implementation of the `reduce` function requires the use of a
operator in our previous case we used the interface
`BiSetCommand<T,S,R>` which we concretize in a class of the form
`BiSetCommand<T,R,R>` where `T` is the type from which we extract a
value value of type `R` and we produce a result of the same type `R`
for that becomes an operator.

Lambda expressions have a similar function that takes care of perform
said task `BiFunction<T,S,R>` which we will handle in this case we
will concretize it to a lambda expression that is an instance of
`BiFunction<SalesSummaryRow, Double, Double>`. In this case the lambda
expression will be as follows:

```java
(s,r) -> s.profit() + r
```

---

---

### Summary of Lambda Way

* As we did in our previous version, lambda expressions they bring a
  series of interfaces that support the different models of functions,
  these are defined in the package: `java.util.function`, among which
  we have used.
* The following is the group of interfaces used.
* `Consumer<T>` represents a computation that receives a value and
   does not produces result.
* `Predicate<T>`represents a computation that receives a value and
   sets a property (predicate).
* `Function<T,R>` represents a computation that receives a value and
   returns a value.
* `BiFunction<T,S,R>` represents a computation that receives two
   parameters and returns a value.
* There are other interfaces in the package that can be used in other
   cases.

---
branch: method_reference
---

### A Lambda Way with Reference Methods

The addition of lambda expressions has substantially improved the
legibility of our project, as well as the elimination of the repeated
code. We can still take advantage of some improvements from the lambda
expressions to make our code more concise.

Method references allow us to do, in many cases the simpler lambda
expressions by replacing them with syntactic sugar that will enable us
to make our code more concise and, therefore more readable.

<!-- TODO - Wrong focus -->
---
focus: src/main/java/com/epam/rd/cls/Main.java:57:old
---

### Method reference: object reference

A method reference is not a pointer to a method, it is a form short of
writing certain lambda expressions (syntactic sugar). The following
lambda expression:

```java
s -> System.out.println(s);
```

Note that the intention is to pass the variable `s` as an argument to
the invocation of the `println` method of the `PrintStream` instance
`System.out`. So this can be expressed more compact:

```java
System.out::println
```

The argument to the `println` method is left implicit and can be
converted inversely to the previous lambda expression.

<!-- TODO - Wrong focus -->
---
focus: src/main/java/com/epam/rd/cls/Main.java:68-71:old
---

### Method reference: class method references

Reference methods are very flexible, allowing you to use object
reference as done on the previous page, as well as we can use specific
class methods. It is very common to find lambda expressions that have
the following format:

```java
s -> s.getInitialUnits()
```

In this case, it differs from the version on the previous page because
here you are referencing a method of the argument, in this case we can
use a different version.

```java
SalesSummaryRow::getInitialUnits
```

This version uses the `getInitialUnits()` method of the class
`SalesSummaryRow`. We can also see this in [Main](focus:
src/main/java/com/epam/rd/cls/Main.java:74-77:old)
with the invocation of the `finalUnits()` method and we also see it in
[Main](focus:
src/main/java/com/epam/rd/cls/Main.java:80-83:old)
for the invocation of the `profit()` method.

<!-- TODO - Wrong focus -->
---
focus: src/main/java/com/epam/rd/cls/Main.java:85-87:old
---

### Method reference: static method references

Method references do not work in all necessary cases and that is why
sometimes it is essential to build functions auxiliaries that allow us
to manipulate the operations in a more simple. The following code
shows a lambda expression with two parameters (`s`, `r`); in the
first, we could use a method reference for the class method, but since
we can use the second parameter, since that method reference is only
set for the `SalesSummaryRow` class.

```java
(s,r) -> s.profit() + r
```

That's why you need to define a static method inside the class
[`Main`](src/main/java/com/epam/rd/cls/Main.java:67-71),
the method in question, `sumProfit` receives both required parameters
and performs the same behavior as the lambda expression above. By So
we already have a static way that can be referenced within the [final
computation of
program](src/main/java/com/epam/rd/cls/Main.java:105-107). The
parameter passed with this form: `Main::sumProfit` is another
reference method, but in this case, with a static method.

<!-- TODO - Wrong focus -->
---
focus: src/main/java/com/epam/rd/cls/Main.java:57-65
---

### Lambda expressions as return values

Sometimes it is necessary to have greater flexibility, for that in
many situations you must have the same method with different
parameters, in this case, we can create dynamic lambda expressions
created on demand, in this case, we have two static methods that the
important thing is the value of the return that in both cases will be
instances of `Predicate`:
[`createPredicateRegion`](src/main/java/com/epam/rd/cls/Main.java:57-60)
to create a predicate that checks the predicate and
[`createPredicateSalesCost`](src/main/java/com/epam/rd/cls/Main.java:62-65)
which allows us to create a predicate that checks the percentage value
between sale and production.

Look at the implementation of the method
[`createPredicateRegion`](src/main/java/com/epam/rd/cls/Main.java:57-60)
this method returns a concretization of `Predicate` in which does a
comparison of the region contained in the variable `s`
(`SalesSummaryRow`) with the value passed to it by the `region`
parameter, this allows you to create dynamic functions that will later
be used as predicates inside some filters:
[filters1](src/main/java/com/epam/rd/cls/Main.java:88-91).
This is a way of creating custom functions from another function.

---

---

###  Summary of A Lambda Way with Reference Methods

Reference methods are excellent complements to produce More compact
code with lambdas expressions. methods of references are neither
references nor pointers. The Reference methods are a plugin, syntactic
sugar for make the code more straightforward, but in the end, they use
expressions lambdas, since the compiler will translate those
references from methods to lambda functions.

There are other method references that we will not show in this case
like:

* References to instance methods of higher classes (`super`).
* Class instance constructor references.
* Array constructor references.

---
focus:
---

### Let Lambdas Streams

Until now, each of the implementations of the base service data can be
viewed as a total computation over the database, but if we look in
more detail inside the code of the same, for example of the `foreach`
service we find that for each element applies a specific computation
to transform its value. that computation, which is currently
represented by a lambda expression, performs The transformation. This
means that our computation behaves as an aggregation operation, what
we have is a set of aggregation operation to generate our specific
service.

This is the same definition of a *stream* in java: "A *stream* is a
sequence of data elements supporting operations of *sequential* and
*parallel* aggregation.

This means that our services can be implemented directly as *stream*.

---
branch: stream
tag: stream-v2.1
focus:
---

### Database service in Streams

The services we implement: `foreach`, `filter`, `map`, `reduce` and
others are already implemented within the *stream*. In In this case,
we are going to start from our database and generate said *stream*:

```java
List<SalesSumaryRow> dbSales = ...;
dbSales.stream(). ...
```

The above implementation produced on each iteration a new database
[`Main`](src/main/java/com/epam/rd/cls/Main.java:88-91:old)
in the new version, a sequence of operations is simply generated, in
the reverse order that we had done previously
[`Main`](src/main/java/com/epam/rd/cls/Main.java:45-42).
Note that each operation directly receives the lambda expression,
represented by the reference method.

---

---

### Conclusions

* We have shown a way of like a fully oriented world to objects we can
  use the characteristics that exist in the functional languages such
  as lambdas expressions.
* Generality not only works in the early stages of object-oriented
  programming but is also the fundamental basis of programming with
  lambdas expressions.
* A language like Java is a language because of a large number of
  builders.
